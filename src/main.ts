import Main from "./Main.svelte"
import "nebulous-svelte-ui/style"

const app = new Main({
    target: document.body
})

export default app
