import { invoke } from "@tauri-apps/api"
import { listen } from "@tauri-apps/api/event"
import { writable, type Readable } from "svelte/store"
import type { Writable } from "svelte/store"
import type {
    BackendInterface,
    Category,
    CategorySelector,
    DecryptedInterface,
    ErrorData,
    Filters,
    Level,
    Log,
    Section,
    Settings,
    Time,
    UpdateData,
} from "nebulous-svelte-ui/src/backendInterface"
import { writeText } from "@tauri-apps/api/clipboard"

function onStoreChange<T>(store: Readable<T>, callback: (v: T) => void) {
    let notFirst = false

    store.subscribe(v => {
        if (notFirst) {
            callback(v)
        } else {
            notFirst = true
        }
    })
}

function backendValue<T, BackendT>(
    defaultVal: T,
    updatedEvent: string,
    parseServerVal: (v: BackendT) => T,
): Readable<T> {
    const store = writable(defaultVal)

    listen<BackendT>(updatedEvent, serverVal => {
        store.set(parseServerVal(serverVal.payload))
    })

    return { subscribe: store.subscribe }
}

interface SectionCategories {
    time: Time
    category_ids: number[]
}

interface RustSection {
    title: string
    categories: SectionCategories
}

export class SectionCategorySelector implements CategorySelector {
    constructor(sectionKey: unknown, categoryIds: number[]) {
        this.categoryIds = writable(categoryIds)
        this.sectionKey = sectionKey
    }

    categoryIds: Writable<number[]>
    sectionKey: unknown

    async addCategory(id: number): Promise<void> {
        let newCategories = await invoke<number[]>("add_category_to_section", {
            sectionKeyEncoded: this.sectionKey,
            category: id,
        })

        this.categoryIds.set(newCategories)
    }

    async removeCategory(id: number): Promise<void> {
        let newCategories = await invoke<number[]>("remove_category_from_section", {
            sectionKeyEncoded: this.sectionKey,
            category: id,
        })

        this.categoryIds.set(newCategories)
    }
}

export class TauriSection implements Section {
    constructor(
        section: RustSection,
        index: unknown,
        deleteSelf: () => Promise<void>,
    ) {
        this.deleteSelf = deleteSelf
        this.key = index
        this.title = writable(section.title)
        this.time = writable(section.categories.time)
        this.categorySelector = new SectionCategorySelector(this.key, section.categories.category_ids)

        onStoreChange(this.title, title => {
            invoke("update_section_title", {
                sectionKeyEncoded: this.key,
                title,
            })
        })

        onStoreChange(this.time, time => {
            invoke("update_section_time", {
                sectionKeyEncoded: this.key,
                time,
            })
        })
    }

    readonly key: unknown
    readonly title: Writable<string>
    readonly time: Writable<Time>
    readonly categorySelector: CategorySelector
    readonly deleteSelf: () => Promise<void>
    readonly suggestions: Writable<number[] | null> = writable(null)

    async getCurrentText(): Promise<string> {
        return invoke<string>("get_text", { sectionKeyEncoded: this.key })
    }

    async updateText(text: string) {
        let maybeData = await invoke<{
            maybeSuggestions: number[] | null
        } | null>("update_section_text", {
            sectionKeyEncoded: this.key,
            text,
        })

        if (maybeData !== null) {
            this.suggestions.set(maybeData.maybeSuggestions)
        }
    }

    async delete() {
        await this.deleteSelf()
    }
}

interface RustCategory {
    id: number
    name: string
    subcategories: RustCategory[]
    color: string
    closed: boolean
}

export class TauriCategory implements Category {
    constructor(
        rustCategory: RustCategory,
        changeParent: (
            childId: number,
            newParentId: number | null,
        ) => Promise<void>,
        deleteImpl: (id: number) => Promise<void>,
    ) {
        this.id = rustCategory.id
        this.name = writable(rustCategory.name)
        this.changeParentImpl = changeParent
        this.deleteImpl = deleteImpl

        this._subcategories = rustCategory.subcategories.map(
            v => new TauriCategory(v, changeParent, deleteImpl),
        )

        this.color = writable(rustCategory.color)
        this.closed = writable(rustCategory.closed)

        onStoreChange(this.name, name => {
            invoke("rename_category", { categoryId: this.id, name })
        })

        onStoreChange(this.color, color => {
            invoke("change_category_color", { categoryId: this.id, color })
        })

        onStoreChange(this.closed, closed => {
            invoke("set_category_closed", {
                categoryId: this.id,
                closed,
            })
        })
    }

    async delete() {
        await this.deleteImpl(this.id)
    }

    async changeParent(newParent: Category | null) {
        await this.changeParentImpl(this.id, newParent?.id ?? null)
    }

    readonly id: number
    readonly name: Writable<string>
    private _subcategories: Category[]
    readonly color: Writable<string>
    readonly closed: Writable<boolean>

    readonly changeParentImpl: (
        newChild: number,
        newParentId: number | null,
    ) => Promise<void>
    readonly deleteImpl: (id: number) => Promise<void>

    get subcategories() {
        return this._subcategories
    }
}

interface RustSettings {
    language: string
    category_prediction: boolean
    check_for_updates: boolean | null
}

export class TauriSettings implements Settings {
    constructor(settings: RustSettings) {
        this.language = writable(settings.language)
        this.categoryPrediction = writable(settings.category_prediction)
        this.checkForUpdates = writable(settings.check_for_updates)

        onStoreChange(this.language, language =>
            invoke("update_language", { language }),
        )

        onStoreChange(this.categoryPrediction, categoryPrediction =>
            invoke("update_category_prediction", { categoryPrediction }),
        )

        onStoreChange(this.checkForUpdates, checkForUpdates =>
            invoke("update_check_for_updates", { checkForUpdates }),
        )
    }

    language: Writable<string>
    categoryPrediction: Writable<boolean>
    checkForUpdates: Writable<boolean | null>
}

interface RustFilters {
    selectedCategories: number[]
    timeOption: Time | null
    textSearch: string
    categoryFilterType: "All" | "Any" | "Not"
}

export class FiltersCategorySelector implements CategorySelector {
    constructor(categoryIds: number[]) {
        this.categoryIds = writable(categoryIds)
    }

    categoryIds: Writable<number[]>

    async addCategory(id: number): Promise<void> {
        let newCategories = await invoke<number[]>("add_category_to_filters", {
            category: id,
        })

        this.categoryIds.set(newCategories)
    }

    async removeCategory(id: number): Promise<void> {
        let newCategories = await invoke<number[]>("remove_category_from_filters", {
            category: id,
        })

        this.categoryIds.set(newCategories)
    }
}

class TauriFilters implements Filters {
    constructor(filters: RustFilters) {
        this.categorySelector = new FiltersCategorySelector(filters.selectedCategories)
        this.timeOption = writable(filters.timeOption)
        this.textSearch = writable(filters.textSearch)
        this.categoryFilterType = writable(filters.categoryFilterType)

        onStoreChange(this.timeOption, timeOption => {
            invoke("update_time_option", { timeOption })
        })

        onStoreChange(this.textSearch, textSearch => {
            invoke("update_text_search", { textSearch })
        })

        onStoreChange(this.categoryFilterType, categoryFilterType => {
            invoke("set_category_filter_option", { categoryFilterType })
        })
    }

    categorySelector: CategorySelector
    timeOption: Writable<Time | null>
    textSearch: Writable<string>
    categoryFilterType: Writable<"All" | "Any" | "Not">
}

interface RustUpdateData {
    version: string
    notes: string | null
}

interface DecryptedInitialData {
    filters: RustFilters
    sections: [unknown, RustSection][]
    categories: RustCategory[]
    sectionsPassingFilters: unknown[]
}

interface Ready {
    settings: RustSettings
}

export async function ready(): Promise<BackendInterface | null> {
    let readyData = await invoke<Ready | null>("ready")

    if (readyData !== null) {
        return new TauriBackendInterface(readyData)
    } else {
        return null
    }
}

export class TauriBackendInterface implements BackendInterface {
    constructor(ready: Ready) {
        listen<boolean>("nebulous://update-encryption", newEncryption => {
            this.encryption.set(newEncryption.payload)
        })

        listen<RustUpdateData>("nebulous://ask-install-update", data => {
            this.update.set({
                installUpdate: () => invoke("install_update"),
                ...data.payload,
            })
        })

        listen<ErrorData>("nebulous://error", error => {
            this.error.set(error.payload)
        })

        listen<Log>("nebulous://log", log => {
            this.logs.update(logs => {
                logs.push(log.payload)
                return logs
            })
        })

        this.settings = new TauriSettings(ready.settings)

        invoke("initialized")
    }

    settings: Settings
    encryption: Writable<boolean | null> = writable(null)
    update: Writable<UpdateData | null> = writable(null)
    error: Writable<ErrorData | null> = writable(null)
    logs: Writable<Log[]> = writable([])

    openDocs(title: string) {
        invoke("open_docs", { title })
    }

    async dataState(): Promise<
        "None" | "Encrypted" | TauriDecryptedInterface | null
    > {
        let result = await invoke<
            "None" | "Encrypted" | { Decrypted: DecryptedInitialData } | null
        >("data_state")

        if (result && typeof result === "object") {
            return new TauriDecryptedInterface(result.Decrypted)
        } else {
            return result
        }
    }

    async createDiary(
        password: string | null,
    ): Promise<TauriDecryptedInterface | null> {
        let result: DecryptedInitialData | null = await invoke("create_diary", {
            password,
        })

        if (result) {
            return new TauriDecryptedInterface(result)
        } else {
            return null
        }
    }

    async decryptDiary(
        password: string,
    ): Promise<TauriDecryptedInterface | "IncorrectKey" | null> {
        let result:
            | "IncorrectKey"
            | { Decrypted: DecryptedInitialData }
            | null = await invoke("decrypt_diary", {
            password,
        })

        if (result !== null && typeof result === "object") {
            return new TauriDecryptedInterface(result.Decrypted)
        } else {
            return result
        }
    }

    async log(level: Level, msg: string): Promise<void> {
        await invoke("log", { level, msg })
    }

    copyToClipboard(data: string) {
        writeText(data)
    }
}

class TauriDecryptedInterface implements DecryptedInterface {
    constructor(data: DecryptedInitialData) {
        this.sections = writable(
            data.sections.map(
                ([key, section]) =>
                    new TauriSection(section, key, this.deleteSectionImpl(key)),
            ),
        )
        this.categories = writable(
            data.categories.map(
                category =>
                    new TauriCategory(
                        category,
                        this.changeParentImpl(),
                        this.deleteCategoryImpl(),
                    ),
            ),
        )
        this.sectionsPassingFilter = backendValue<unknown[], unknown[]>(
            data.sectionsPassingFilters,
            "nebulous://updated-sections-passing-filter",
            v => v,
        )

        this.filters = new TauriFilters(data.filters)
    }

    filters: Filters
    sections: Writable<Section[]>
    categories: Writable<Category[]>
    sectionsPassingFilter: Readable<unknown[]>

    async createCategory(name: string) {
        let categories = await invoke<RustCategory[] | null>(
            "create_category",
            { name },
        )

        if (categories) {
            this.categories.set(
                categories.map(
                    category =>
                        new TauriCategory(
                            category,
                            this.changeParentImpl(),
                            this.deleteCategoryImpl(),
                        ),
                ),
            )
        }
    }

    async createSection() {
        let maybeSection = await invoke<{
            section: RustSection
            key: unknown
        } | null>("create_section")

        let section: RustSection
        let key: unknown

        if (maybeSection !== null) {
            section = maybeSection.section
            key = maybeSection.key
        } else {
            return
        }

        this.sections.update(sections => {
            sections.unshift(
                new TauriSection(section, key, this.deleteSectionImpl(key)),
            )

            return sections
        })
    }

    async changeEncryption(newPassword: string | null) {
        await invoke("change_password", { newPassword })
    }

    deleteSectionImpl(key: unknown): () => Promise<void> {
        return async () => {
            await invoke("delete_section", {
                sectionKeyEncoded: key,
            })

            this.sections.update(sections =>
                sections.filter(section => section.key !== key),
            )
        }
    }

    changeParentImpl(): (
        childId: number,
        newParentId: number | null,
    ) => Promise<void> {
        let self = this // `this` gets changed when the function gets assigned to stuff

        return async (childId: number, newParentId: number | null) => {
            let categories = await invoke<RustCategory[] | null>(
                "change_category_parent",
                {
                    newChildId: childId,
                    newParentId: newParentId,
                },
            )

            if (categories) {
                this.categories.set(
                    categories.map(
                        category =>
                            new TauriCategory(
                                category,
                                self.changeParentImpl(),
                                self.deleteCategoryImpl(),
                            ),
                    ),
                )
            }
        }
    }

    deleteCategoryImpl(): (id: number) => Promise<void> {
        let self = this // `this` gets changed when the function gets assigned to stuff

        return async (id: number) => {
            let categories = await invoke<RustCategory[] | null>(
                "delete_category",
                {
                    categoryId: id,
                },
            )

            if (categories) {
                this.categories.set(
                    categories.map(
                        category =>
                            new TauriCategory(
                                category,
                                self.changeParentImpl(),
                                self.deleteCategoryImpl(),
                            ),
                    ),
                )
            }
        }
    }
}
