use anyhow::anyhow;
use chrono::Utc;
use clap::{Parser, Subcommand};
use indoc::formatdoc;
use serde::Deserialize;
use serde_json::json;
use std::{
    env,
    fs::{self, DirEntry},
    path::{Path, PathBuf},
    process::Command,
    time::Duration,
};

#[derive(Parser)]
struct Cli {
    #[command(subcommand)]
    command: Commands,
}

#[derive(Subcommand)]
enum Commands {
    Distribute { notes: String },
    Build,
}

fn main() -> Result<(), anyhow::Error> {
    let cli = Cli::parse();

    match cli.command {
        Commands::Distribute { notes } => distribute(notes),
        Commands::Build => build(),
    }
}

fn build() -> Result<(), anyhow::Error> {
    let current_dir = env::current_dir()?;

    env::set_current_dir("../")?;
    Command::new("npm").args(["update"]).spawn()?.wait()?;

    env::set_current_dir("src-tauri")?;
    Command::new("cargo").args(["update"]).spawn()?.wait()?;

    env::set_current_dir("../")?;

    let password = rpassword::prompt_password("Password: ")?;

    env::set_var("TAURI_KEY_PASSWORD", password);
    env::set_var("TAURI_PRIVATE_KEY", include_str!("../../signing.key"));

    Command::new("cargo")
        .args(["tauri", "build"])
        .spawn()?
        .wait()?;

    #[cfg(target_os = "linux")]
    {
        move_files(
            "./src-tauri/target/release/bundle/appimage",
            "./update-data/linux",
            &["AppImage", "gz", "sig"],
            true,
        )?;
    }

    #[cfg(target_os = "windows")]
    {
        move_files(
            "src-tauri\\target\\release\\bundle\\msi",
            "Z:\\nebulous-bundle", // Shared folder in Boxes
            &["msi", "zip", "sig"],
            true,
        )?;
    }

    #[cfg(target_os = "macos")]
    {
        move_files(
            "./src-tauri/target/release/bundle/dmg",
            "./update-data/macos",
            &["dmg"],
            true,
        )?;
        move_files(
            "./src-tauri/target/release/bundle/macos",
            "./update-data/macos",
            &["gz", "sig"],
            false,
        )?;
    }

    env::set_current_dir(current_dir)?;

    Ok(())
}

fn move_files(
    from_dir: &str,
    to_dir: &str,
    file_extenstions: &[&str],
    clear: bool,
) -> Result<(), anyhow::Error> {
    let from_dir_entries = fs::read_dir(PathBuf::from(from_dir))?.collect::<Result<Vec<_>, _>>()?;

    let to_dir = PathBuf::from(to_dir);

    if clear {
        for to_delete in fs::read_dir(&to_dir)? {
            fs::remove_file(to_delete?.path())?;
        }
    }

    for file in file_extenstions
        .iter()
        .map(|extension| find_path(&from_dir_entries, extension))
    {
        let name = file.file_name().unwrap();
        let to = to_dir.join(name);
        if fs::rename(&file, &to).is_err() {
            fs::copy(&file, to)?;
        }
    }

    Ok(())
}

fn distribute(notes: String) -> Result<(), anyhow::Error> {
    let manifest = cargo_toml::Manifest::from_path("../src-tauri/Cargo.toml")?;

    let version = manifest
        .package
        .expect("Package section should be there")
        .version
        .unwrap();

    let linux_files = fs::read_dir("linux")?.collect::<Result<Vec<_>, _>>()?;

    let appimage_path = find_path(&linux_files, "AppImage");
    let appimage_zip_path = find_path(&linux_files, "gz");
    let appimage_sig_path = find_path(&linux_files, "sig");

    let appimage = upload_to_gitlab(&appimage_path, "linux", &version)?;
    let appimage_zip = upload_to_gitlab(&appimage_zip_path, "linux", &version)?;

    println!("Linux release: {}", &appimage);

    let macos_files = fs::read_dir("macos")?.collect::<Result<Vec<_>, _>>()?;

    let dmg_path = find_path(&macos_files, "dmg");
    let dmg_zip_path = find_path(&macos_files, "gz");
    let dmg_sig_path = find_path(&macos_files, "sig");

    let dmg = upload_to_gitlab(&dmg_path, "macos", &version)?;
    let dmg_zip = upload_to_gitlab(&dmg_zip_path, "macos", &version)?;

    println!("MacOS release: {}", &dmg);

    let windows_files = fs::read_dir("windows")?.collect::<Result<Vec<_>, _>>()?;

    let msi_path = find_path(&windows_files, "msi");
    let msi_zip_path = find_path(&windows_files, "zip");
    let msi_sig_path = find_path(&windows_files, "sig");

    let msi = upload_to_gitlab(&msi_path, "windows", &version)?;
    let msi_zip = upload_to_gitlab(&msi_zip_path, "windows", &version)?;

    println!("Windows release: {}", &msi);

    let current_update_json = format!(
        r#"{{
            "version": "{version}",
            "notes": "{notes}",
            "pub_date": "{pub_date}",
            "platforms": {{
                "linux-x86_64": {{
                    "signature": "{appimage_signature}",
                    "url": "{appimage_url}"
                }},
                "darwin-x86_64": {{
                    "signature": "{dmg_signature}",
                    "url": "{dmg_url}"
                }},
                "windows-x86_64": {{
                    "signature": "{msi_signature}",
                    "url": "{msi_url}"
                }}
            }}
        }}"#,
        version = version,
        notes = notes,
        pub_date = Utc::now().format("%Y-%m-%dT%H:%M:%SZ"),
        appimage_signature = fs::read_to_string(appimage_sig_path)?,
        appimage_url = &appimage_zip,
        dmg_signature = fs::read_to_string(dmg_sig_path)?,
        dmg_url = &dmg_zip,
        msi_signature = fs::read_to_string(msi_sig_path)?,
        msi_url = &msi_zip,
    );

    fs::write("current_update.json", current_update_json)?;

    let client = reqwest::blocking::Client::new();
    let res = client
        .post("https://gitlab.com/api/v4/projects/36609778/releases")
        .header("PRIVATE-TOKEN", include_str!("gitlab-token.txt"))
        .json(&json!({
            "name": version,
            "tag_name": version,
            "ref": "main",
            "description": formatdoc!(r#"
                    {notes}

                    ## Download
                    - [Linux]({appimage})
                    - [Windows]({msi})
                    - [MacOS]({dmg})
                "#),
            "assets": {
                "links": [
                    { "name": "Linux", "url": appimage },
                    { "name": "Windows", "url": msi },
                    { "name": "MacOS", "url": dmg },
                ],
            },
        }))
        .send()?;

    if !res.status().is_success() {
        return Err(anyhow!("Failed to create a release: {}", res.text()?));
    }

    Ok(())
}

fn find_path(entries: &[DirEntry], ext: &str) -> PathBuf {
    let path = entries
        .iter()
        .find(|v| v.path().extension() == Some(ext.as_ref()))
        .unwrap()
        .path();

    println!("Got {path:?}");

    path
}

#[derive(Deserialize)]
struct UploadResponse {
    id: u64,
}

fn upload_to_gitlab(
    file_path: &Path,
    platform: &str,
    version: &str,
) -> Result<String, anyhow::Error> {
    let file_name = file_path
        .file_name()
        .ok_or_else(|| anyhow!("The file name must exist"))?
        .to_str()
        .ok_or_else(|| anyhow!("The file name must be utf8"))?;

    let request = format!("https://gitlab.com/api/v4/projects/36609778/packages/generic/{platform}/{version}/{file_name}?select=package_file");

    println!("Uploading {request}");

    let client = reqwest::blocking::Client::new();
    let res = client
        .put(request)
        .header("PRIVATE-TOKEN", include_str!("gitlab-token.txt"))
        .body(fs::read(file_path)?)
        .timeout(Duration::from_secs(10000))
        .send()?;

    if res.status().is_success() {
        let UploadResponse { id } = res.json()?;

        Ok(format!(
            "https://gitlab.com/nebulous-app/nebulous/-/package_files/{id}/download"
        ))
    } else {
        Err(anyhow!("Request failed: {}", res.text()?))
    }
}
