use std::{fs, process::Command};

fn main() {
    println!("cargo:rerun-if-changed=../docs");

    if let Err(e) = build_books() {
        println!("cargo:warning={}", e);
    }

    tauri_build::build()
}

fn build_books() -> Result<(), String> {
    let dirs = fs::read_dir("../docs").map_err(|e| e.to_string())?;

    for maybe_folder in dirs {
        let folder = maybe_folder.map_err(|e| e.to_string())?;

        // .DS_STORE >:(
        if !folder.path().is_dir() {
            continue;
        }

        let output = Command::new("mdbook")
            .args(["build".as_ref(), folder.path().as_os_str()])
            .output()
            .map_err(|e| e.to_string())?;

        if !output.status.success() {
            return Err(format!(
                "Building book failed: {}",
                String::from_utf8(output.stderr).unwrap()
            ));
        }
    }

    Ok(())
}
