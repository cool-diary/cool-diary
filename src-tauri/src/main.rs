#![cfg_attr(
    all(not(debug_assertions), target_os = "windows"),
    windows_subsystem = "windows"
)]
#![warn(clippy::cognitive_complexity)]

mod ipc;
mod logger;
mod settings;

use std::{sync::Arc, time::Duration};

use electra::{BufferingStorageAccess, DiskStorageAccess, Manager as FileManager};
use error_stack::{Report, ResultExt};
use error_stack_error_macro::error;
use lib_nebulous::{Config, DiaryState};
use log::{info, Level};
use logger::Logger;
use settings::Settings;
use tauri::{Manager, Wry};
use tokio::sync::{mpsc, RwLock};

use crate::ipc::{
    add_category_to_filters, add_category_to_section, change_category_color,
    change_category_parent, change_password, create_category, create_diary, create_section,
    data_state, decrypt_diary, delete_category, delete_section, get_text, initialized,
    install_update, log, open_docs, ready, remove_category_from_filters,
    remove_category_from_section, rename_category, set_category_closed, set_category_filter_option,
    show_error, update_category_prediction, update_check_for_updates, update_language,
    update_section_text, update_section_time, update_section_title, update_text_search,
    update_time_option,
};

error!(pub StartupError; "Failed to start up the app");

static LOGGER: Logger<Wry> = Logger::new(Level::Trace);

fn main() {
    LOGGER.install();

    info!("Starting Nebulous");

    tauri::Builder::default()
        .setup(|app| {
            let runtime_handle = tauri::async_runtime::handle().inner().to_owned();

            let data_dir = tauri::api::path::data_dir()
                .ok_or_else(|| {
                    format!(
                        "{:?}",
                        Report::new(StartupError).attach_printable("Failed to get data directory")
                    )
                })?
                .join("nebulous");

            let (tx, mut rx) = mpsc::unbounded_channel();

            let config_storage_access = Arc::new(
                DiskStorageAccess::new(data_dir.clone())
                    .change_context(StartupError)
                    .map_err(|e| format!("{e:?}"))?,
            );

            let mut config = RwLock::new(
                tauri::async_runtime::block_on(FileManager::<Config<Settings>, _>::new(
                    Arc::clone(&config_storage_access),
                    &(),
                    (),
                    (),
                ))
                .change_context(StartupError)
                .map_err(|e| format!("{e:?}"))
                .and_then(|v| match v {
                    Some(config) => Ok(config),
                    None => {
                        let mut config = Config::new(60 * 60 * 24, Settings::default());

                        config
                            .diaries_mut()
                            .push(("Diary".to_owned(), data_dir.join("diary")));

                        tauri::async_runtime::block_on(FileManager::new_from_data(
                            Arc::clone(&config_storage_access),
                            &(),
                            config,
                            (),
                            (),
                        ))
                        .map_err(|e| format!("{e:?}"))
                    }
                })?,
            );

            let training_interval = config.get_mut().training_interval();

            #[cfg(all(feature = "auto-updates", not(debug_assertions)))]
            {
                use serde_json::json;

                if config.get_mut().custom().check_for_updates == Some(true) {
                    let app_handle = app.handle();

                    runtime_handle.spawn(async move {
                        match app_handle
                            .updater()
                            // Updates take their sweet time for some reason
                            .timeout(Duration::from_secs(1000))
                            .check()
                            .await
                        {
                            Ok(update) => {
                                if update.is_update_available() {
                                    if let Err(e) = app_handle.emit_all(
                                        "nebulous://ask-install-update",
                                        json!({
                                            "version": update.latest_version(),
                                            "notes": update.body(),
                                        }),
                                    ) {
                                        show_error(&app_handle, Report::new(e))
                                    };
                                    app_handle.manage(update);
                                }
                            }
                            Err(e) => show_error(&app_handle, Report::new(e)),
                        }
                    });
                }
            }

            let diary = config.get_mut().diaries().first().ok_or_else(|| {
                format!(
                    "{:?}",
                    Report::new(StartupError).attach_printable("There are no diaries")
                )
            })?;

            let diary_vfs = Arc::new(BufferingStorageAccess::new(
                Arc::new(
                    DiskStorageAccess::new(diary.1.to_owned())
                        .change_context(StartupError)
                        .map_err(|e| format!("{e:?}"))?,
                ),
                runtime_handle.to_owned(),
                Duration::from_secs(1),
                tx,
            ));

            app.manage(config);

            let app_handle = app.handle();

            let error_report_handle = app_handle.clone();

            runtime_handle.spawn(async move {
                while let Some(error) = rx.recv().await {
                    show_error(&error_report_handle, error);
                }
            });

            let handle_for_err = app_handle.clone();

            let diary_state = RwLock::new(
                runtime_handle
                    .block_on(DiaryState::get(
                        diary_vfs,
                        training_interval,
                        move |err| show_error(&handle_for_err, err),
                        runtime_handle.to_owned(),
                    ))
                    .change_context(StartupError)
                    .map_err(|e| format!("{e:?}"))?,
            );

            app_handle.manage(diary_state);

            let main_window = app.get_window("main").ok_or_else(|| {
                format!(
                    "{:?}",
                    Report::new(StartupError).attach_printable("Failed to get the main window")
                )
            })?;

            main_window.show()?;

            info!("Finished application initialization");

            Ok(())
        })
        .invoke_handler(tauri::generate_handler![
            ready,
            open_docs,
            data_state,
            create_diary,
            decrypt_diary,
            change_password,
            create_category,
            change_category_parent,
            change_category_color,
            rename_category,
            delete_category,
            set_category_closed,
            add_category_to_filters,
            remove_category_from_filters,
            update_time_option,
            update_text_search,
            set_category_filter_option,
            create_section,
            delete_section,
            update_section_text,
            update_section_title,
            update_section_time,
            add_category_to_section,
            remove_category_from_section,
            update_language,
            update_category_prediction,
            update_check_for_updates,
            install_update,
            log,
            initialized,
            get_text,
        ])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}
