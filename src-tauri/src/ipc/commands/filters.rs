use error_stack::ResultExt;
use error_stack_error_macro::error;
use lib_nebulous::{CategoryFilterType, Time};
use log::trace;
use serde_json::{json, Value};
use tauri::AppHandle;

use crate::{ipc::updated_filters_convenient as updated_filters, update_meta_secure};

error!(pub AddCategoryToFiltersFailed; "Failed to add a category to the filters");

#[tauri::command]
pub async fn add_category_to_filters(handle: AppHandle, category: u32) -> Option<Value> {
    trace!("Frontend wants to change the section categories");

    let categories = update_meta_secure!(
        &handle,
        AddCategoryToFiltersFailed,
        |decrypted, meta_secure| {
            meta_secure
                .filters
                .category_selector_mut()
                .add_category(category, &meta_secure.categories);

            Ok(json!(meta_secure.filters.category_selector()))
        }
    );

    updated_filters(&handle).await;

    categories
}

error!(pub RemoveCategoryFromFiltersFailed; "Failed to remove a category from the filters");

#[tauri::command]
pub async fn remove_category_from_filters(handle: AppHandle, category: u32) -> Option<Value> {
    trace!("Frontend wants to change the section categories");

    let categories = update_meta_secure!(
        &handle,
        RemoveCategoryFromFiltersFailed,
        |decrypted, meta_secure| {
            meta_secure
                .filters
                .category_selector_mut()
                .remove_category(category);

            Ok(json!(meta_secure.filters.category_selector()))
        }
    );

    updated_filters(&handle).await;

    categories
}

error!(pub UpdateTimeOptionFailed; "Updating time option failed");

#[tauri::command]
pub async fn update_time_option(handle: AppHandle, time_option: Option<Time>) {
    trace!("Frontend wants to change time filtering");

    update_meta_secure!(&handle, UpdateTimeOptionFailed, |decrypted, meta_secure| {
        meta_secure.filters.set_time(time_option);

        Ok(())
    });

    updated_filters(&handle).await;
}

error!(pub UpdateTextSearchFailed; "Updating text search failed");

#[tauri::command]
pub async fn update_text_search(handle: AppHandle, text_search: String) {
    trace!("Frontend wants to change the text search");

    update_meta_secure!(&handle, UpdateTextSearchFailed, |decrypted, meta_secure| {
        meta_secure.filters.set_text_search(&text_search);

        Ok(())
    });

    updated_filters(&handle).await;
}

error!(pub UpdateCategoryFilterTypeFailed; "Updating category filter type failed");

#[tauri::command]
pub async fn set_category_filter_option(
    handle: AppHandle,
    category_filter_type: CategoryFilterType,
) {
    trace!("Frontend wants to set the category filter option");

    update_meta_secure!(
        &handle,
        UpdateCategoryFilterTypeFailed,
        |decrypted, meta_secure| {
            meta_secure
                .filters
                .set_category_filter_type(category_filter_type);

            Ok(())
        }
    );

    updated_filters(&handle).await;
}
