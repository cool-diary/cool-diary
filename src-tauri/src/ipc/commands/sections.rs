use std::{
    path::Path,
    sync::Arc,
    time::{SystemTime, UNIX_EPOCH},
};

use electra::{LazyManager, Manager, Reference, StorageAccess};
use error_stack::{IntoReport, Report, Result, ResultExt};
use error_stack_error_macro::error;
use lib_nebulous::{Media, MediaEdits, RandomFileKey, Section, Text, Time};
use log::trace;
use serde_json::{json, Value};
use tauri::AppHandle;

use crate::{
    ipc::{send_error_to_frontend, updated_filters},
    try_get_decrypted, update_decrypted,
};

error!(DecodeSectionKeyFailed; "Failed to decode a section key");

fn decode_section_key<SA: StorageAccess<FileKey = Reference<Path>, DataType = Vec<u8>>>(
    encoded: &str,
) -> Result<RandomFileKey<Section<SA>>, DecodeSectionKeyFailed> {
    serde_json::from_str::<RandomFileKey<Section<SA>>>(encoded)
        .into_report()
        .change_context(DecodeSectionKeyFailed)
}

fn now() -> u64 {
    SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .expect("the current time to be after the unix epoch")
        .as_secs()
}

error!(pub SectionKeyDoesntExist; "The section key doesn't exist");

#[macro_export]
macro_rules! update_section {
    ($handle: expr, $context: expr, $encoded: expr, |$decrypted: ident, $section: ident$(, $section_key: ident)?| {$($stuff: tt)*}) => {
        update_decrypted!($handle, $context, |$decrypted| {
            let section_key = decode_section_key($encoded)
                .change_context($context)?;

            let section_managed = $decrypted
                .sections
                .get_mut(&section_key)
                .ok_or_else(|| Report::new($crate::ipc::SectionKeyDoesntExist))
                .change_context($context)?;

            section_managed
                .maybe_get_mut()
                .expect("the section to have been read when the diary was decrypted")
                .write($decrypted.maybe_key.to_owned(), |mut $section| async {
                    $(
                        let $section_key = section_key;
                    )?

                    let v = { $($stuff)* };

                    ($section, v)
                })
                .await
                .change_context($context)
        })
    };

    ($handle: expr, $context: expr, $encoded: expr, |$decrypted: ident, $section: ident$(, $section_key: ident)?| $stuff: expr) => {
        update_section!($handle, $context, $encoded, |$decrypted, $section$(, $section_key: ident)?| {$stuff})
    }
}

error!(pub CreateSectionFailed; "Creating section failed");

#[tauri::command]
pub async fn create_section(handle: AppHandle) -> Option<serde_json::Value> {
    trace!("Frontend wants to create a section");

    update_decrypted!(&handle, CreateSectionFailed, |decrypted| {
        let meta_secure = &*decrypted.meta_secure;
        let sa = Arc::clone(&decrypted.storage_access);

        let random_file_key =
            RandomFileKey::generate(&*decrypted.storage_access, decrypted.maybe_key.to_owned())
                .await
                .change_context(CreateSectionFailed)?;

        let mut section = Section::new(
            Arc::clone(&sa),
            &meta_secure.filters,
            &decrypted.sections,
            decrypted.maybe_key.to_owned(),
        )
        .await
        .change_context(CreateSectionFailed)?;

        section.add_file(Media::Text(
            MediaEdits::new(
                Arc::clone(&sa),
                decrypted.maybe_key.to_owned(),
                now(),
                Text::from(String::new()),
                decrypted.maybe_key.to_owned(),
            )
            .await
            .change_context(CreateSectionFailed)?,
        ));

        let ret = json!({
            "section": {
                "title": &section.title(),
                "categories": &section.categories()
            },
            "key": serde_json::to_string(&random_file_key).into_report().change_context(CreateSectionFailed)?,
        });

        decrypted.sections.insert(
            random_file_key,
            LazyManager::from_manager(
                random_file_key,
                Manager::new_from_data(
                    Arc::clone(&sa),
                    &random_file_key,
                    section,
                    decrypted.maybe_key.to_owned(),
                    decrypted.maybe_key.to_owned(),
                )
                .await
                .change_context(CreateSectionFailed)?,
            ),
        );

        updated_filters(
            &handle,
            meta_secure,
            &decrypted.sections,
            sa,
            decrypted.maybe_key.to_owned(),
        )
        .await
        .change_context(CreateSectionFailed)?;

        Ok(ret)
    })
}

error!(pub GetSectionFailed; "Failed to get the text of a section");

#[tauri::command]
pub async fn get_text(handle: AppHandle, section_key_encoded: String) -> Option<String> {
    trace!("Frontend wants to get the text of a section");

    send_error_to_frontend(&handle, async {
        try_get_decrypted!(&handle, assign to decrypted, GetSectionFailed);

        let section_key =
            decode_section_key(&section_key_encoded).change_context(GetSectionFailed)?;

        let file = decrypted
            .sections
            .get(&section_key)
            .ok_or_else(|| {
                Report::new(GetSectionFailed).attach_printable("The section key doesn't exist")
            })?
            .maybe_get()
            .expect("the section to have been read on decrypt")
            .get_file(0)
            .ok_or_else(|| {
                Report::new(GetSectionFailed)
                    .attach_printable("The section doesn't contain any files")
            })?;

        let key = decrypted.maybe_key.to_owned();

        match file {
            Media::Text(text) => text
                .latest(Arc::clone(&decrypted.storage_access), key.to_owned(), key)
                .await
                .change_context(GetSectionFailed)
                .map(|v| (*v).to_owned()),
        }
    })
    .await
}

error!(pub DeleteSectionFailed; "Failed to delete section");

#[tauri::command]
pub async fn delete_section(handle: AppHandle, section_key_encoded: String) {
    trace!("Frontend wants to delete a section");

    update_decrypted!(&handle, DeleteSectionFailed, |decrypted| {
        let section_key =
            decode_section_key(&section_key_encoded).change_context(DeleteSectionFailed)?;

        let mut section = decrypted.sections.remove(&section_key).ok_or_else(|| {
            Report::new(DeleteSectionFailed)
                .attach_printable("The key wasn't found in the list of sections")
        })?;

        let key = decrypted.maybe_key.to_owned();

        section
            .maybe_get_mut()
            .expect("the section to have been read when decrypting diary")
            .write(key.to_owned(), |mut section| async {
                let ret = section
                    .remove_all_files(&decrypted.storage_access, key.to_owned())
                    .await
                    .change_context(DeleteSectionFailed);

                (section, ret)
            })
            .await
            .change_context(DeleteSectionFailed)??;

        section
            .delete(&decrypted.storage_access, key)
            .await
            .change_context(DeleteSectionFailed)
    });
}

error!(pub UpdateSectionTextFailed; "Failed to update the section's text");

#[tauri::command]
pub async fn update_section_text(
    handle: AppHandle,
    section_key_encoded: String,
    text: String,
) -> Option<serde_json::Value> {
    trace!("Frontend wants to change the section text");

    update_decrypted!(&handle, UpdateSectionTextFailed, |decrypted| {
        let maybe_model_manager = decrypted.model.read().await;
        let maybe_model = (*maybe_model_manager).as_ref();

        let section_key =
            decode_section_key(&section_key_encoded).change_context(UpdateSectionTextFailed)?;

        let section_managed = decrypted
            .sections
            .get_mut(&section_key)
            .ok_or_else(|| Report::new(crate::ipc::SectionKeyDoesntExist))
            .change_context(UpdateSectionTextFailed)?
            .maybe_get_mut()
            .expect("the section to have been read on decrypt");

        let mut maybe_suggestions = None;

        let sa = Arc::clone(&decrypted.storage_access);
        let key = decrypted.maybe_key.to_owned();

        section_managed
            .write(key.to_owned(), |mut section| async {
                match section.update_file(0) {
                    Some(file) => match file {
                        Media::Text(text_file) => {
                            if let Err(e) = text_file
                                .update(
                                    Text::from(text),
                                    now(),
                                    Arc::clone(&sa),
                                    key.to_owned(),
                                    key.to_owned(),
                                )
                                .await
                            {
                                return (section, Err(e.change_context(UpdateSectionTextFailed)));
                            }
                        }
                    },
                    None => {
                        return (
                            section,
                            Err(Report::new(UpdateSectionTextFailed)
                                .attach_printable("There are no text files on the section")),
                        )
                    }
                }

                if let Some(model) = maybe_model {
                    maybe_suggestions = Some(match model.predict(sa, key, &section).await {
                        Ok(v) => v,
                        Err(e) => return (section, Err(e.change_context(UpdateSectionTextFailed))),
                    });
                }

                (section, Ok::<_, Report<_>>(()))
            })
            .await
            .change_context(UpdateSectionTextFailed)??;

        Ok(json!({ "maybeSuggestions": maybe_suggestions }))
    })
    // Don't update filters so the section someone is editing doesn't disappear while editing
}

error!(pub UpdateSectionTitleFailed; "Update section title failed");

#[tauri::command]
pub async fn update_section_title(handle: AppHandle, section_key_encoded: String, title: String) {
    trace!("Frontend wants to change the section title");

    update_section!(
        &handle,
        UpdateSectionTitleFailed,
        &section_key_encoded,
        |decrypted, section| section.set_title(title)
    );
    // Don't update filters so the section someone is editing doesn't disappear while editing
}

error!(pub UpdateSectionTimeFailed; "Update section time failed");

#[tauri::command]
pub async fn update_section_time(handle: AppHandle, section_key_encoded: String, time: Time) {
    trace!("Frontend wants to change the section time");

    update_section!(
        &handle,
        UpdateSectionTimeFailed,
        &section_key_encoded,
        |decrypted, section| section.categories_mut().set_time(time)
    );
    // Don't update filters so the section someone is editing doesn't disappear while editing
}

error!(pub AddCategoryToSectionFailed; "Failed to add a category to a section");

#[tauri::command]
pub async fn add_category_to_section(
    handle: AppHandle,
    section_key_encoded: String,
    category: u32,
) -> Option<Value> {
    trace!("Frontend wants to change the section categories");

    update_section!(
        &handle,
        AddCategoryToSectionFailed,
        &section_key_encoded,
        |decrypted, section| {
            section
                .categories_mut()
                .category_selector_mut()
                .add_category(category, &decrypted.meta_secure.categories);

            json!(section.categories_mut().category_selector())
        }
    )
    // Don't update filters so the section someone is editing doesn't disappear while editing
}

error!(pub RemoveCategoryFromSectionFailed; "Failed to add a category to a section");

#[tauri::command]
pub async fn remove_category_from_section(
    handle: AppHandle,
    section_key_encoded: String,
    category: u32,
) -> Option<Value> {
    trace!("Frontend wants to change the section categories");

    update_section!(
        &handle,
        RemoveCategoryFromSectionFailed,
        &section_key_encoded,
        |decrypted, section| {
            section
                .categories_mut()
                .category_selector_mut()
                .remove_category(category);

            json!(section.categories_mut().category_selector())
        }
    )
    // Don't update filters so the section someone is editing doesn't disappear while editing
}
