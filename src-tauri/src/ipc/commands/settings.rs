use error_stack::{Report, ResultExt};
use error_stack_error_macro::error;
use log::trace;
use tauri::AppHandle;

use crate::settings::Language;

#[macro_export]
macro_rules! update_config {
    ($handle: expr, $context: expr, |$config: ident| {$($stuff: tt)*}) => {
        $crate::ipc::util::send_error_to_frontend($handle, async {
            let mut config = $crate::ipc::util::config($handle).write().await;

            config.write((), |mut $config| async {
                let v = $($stuff)*;

                ($config, Ok(v))
            })
            .await
            .change_context($context)?
        })
        .await
    };
}

error!(pub UpdateLanguageFailed; "Failed to update the language");

#[tauri::command]
pub async fn update_language(handle: AppHandle, language: Language) {
    trace!("Frontend wants to change the language to {language:?}");

    update_config!(&handle, UpdateLanguageFailed, |config| {
        config.custom_mut().language = language
    });
}

error!(pub UpdateCategoryPredictionFailed; "Failed to update whether category prediction is enabled");

#[tauri::command]
pub async fn update_category_prediction(handle: AppHandle, category_prediction: bool) {
    trace!("Frontend wants to change category prediction to {category_prediction}");

    update_config!(&handle, UpdateCategoryPredictionFailed, |config| {
        config.custom_mut().category_prediction = category_prediction
    });
}

error!(pub UpdateCheckForUpdatesFailed; "Failed to update the language");

#[tauri::command]
pub async fn update_check_for_updates(handle: AppHandle, check_for_updates: bool) {
    trace!("Frontend wants to change checking for updates to {check_for_updates}");

    update_config!(&handle, UpdateCheckForUpdatesFailed, |config| {
        match cfg!(feature = "auto-updates") {
            true => config.custom_mut().check_for_updates = Some(check_for_updates),
            false => return (config, Err(Report::new(UpdateCheckForUpdatesFailed).attach_printable("This build doesn't support automatic update checking (it was indended to be uploaded to a package repository)")))
        }
    });
}
