use std::sync::Arc;

use error_stack::{IntoReport, Report, ResultExt};
use error_stack_error_macro::error;
use lib_nebulous::{encryption_data_from_password, DiaryState, IncorrectKey};
use log::info;
use secrecy::{ExposeSecret, Secret};
use serde::Serialize;
use serde_json::json;
use tauri::AppHandle;

use crate::{
    ipc::{
        config, created_meta_secure, default_meta_secure, diary_state, send_error_to_frontend,
        show_error, updated_encryption,
    },
    LOGGER,
};

#[tauri::command]
pub async fn ready(handle: AppHandle) -> serde_json::Value {
    info!("Sending initialization data to the frontend");

    let config = config(&handle).read().await;

    json!({
        "settings": config.custom(),
    })
}

#[tauri::command]
pub async fn initialized(handle: AppHandle) {
    LOGGER.set_app_handle(handle);
}

error!(pub OpenDocsError; "Failed to open the documentation");

#[tauri::command]
pub async fn open_docs(handle: AppHandle, title: String) {
    info!("Opening the documentation");

    send_error_to_frontend(&handle, async {
        let config = config(&handle).read().await;

        let docs = config.custom().language.docs();

        tauri::WindowBuilder::new(&handle, title, docs)
            .build()
            .into_report()
            .change_context(OpenDocsError)
    })
    .await;
}

error!(pub DataStateError; "Failed to get data state");

#[derive(Serialize)]
pub enum DataStateResponse {
    None,
    Encrypted,
    Decrypted(serde_json::Value),
}

#[tauri::command]
pub async fn data_state(handle: AppHandle) -> Option<DataStateResponse> {
    info!("Frontend wants to get the current data state");

    send_error_to_frontend(&handle, async {
        let state = diary_state(&handle).read().await;

        Ok(match &*state {
            lib_nebulous::DiaryState::None(_) => DataStateResponse::None,
            lib_nebulous::DiaryState::Encrypted(_) => DataStateResponse::Encrypted,
            lib_nebulous::DiaryState::Decrypted(decrypted) => DataStateResponse::Decrypted(
                created_meta_secure(
                    &handle,
                    &decrypted.meta_secure,
                    &decrypted.sections,
                    Arc::clone(&decrypted.storage_access),
                    decrypted.maybe_key.to_owned(),
                )
                .await
                .change_context(DataStateError)?,
            ),
        })
    })
    .await
}

#[macro_export]
macro_rules! try_with_original {
    ($stuff: expr, $original: expr, $context: ident) => {
        match $stuff {
            Ok(v) => v,
            Err(e) => return Err(($original, e.change_context($context))),
        }
    };
}

error!(pub DiaryExists; "Diary already exists");
error!(pub CreateDiaryError; "Failed to create diary");

#[tauri::command]
pub async fn create_diary(
    handle: AppHandle,
    password: Option<Secret<String>>,
) -> Option<serde_json::Value> {
    info!(
        "Frontend wants to create a diary with {} password",
        match password {
            Some(_) => "a",
            None => "no",
        }
    );

    send_error_to_frontend(&handle, async {
        let mut state = diary_state(&handle).write().await;

        let mut ret = None;

        state
            .update(|state| async {
                match state {
                    DiaryState::None(no_diary) => {
                        let encryption_data = match password {
                            Some(passwd) => Some(try_with_original!(
                                encryption_data_from_password(
                                    &no_diary.handle,
                                    Secret::new(passwd.expose_secret().as_bytes().to_vec()),
                                    32
                                )
                                .await,
                                DiaryState::None(no_diary),
                                CreateDiaryError
                            )),
                            None => None,
                        };

                        let decrypted = no_diary
                            .create(
                                encryption_data,
                                default_meta_secure(),
                                tauri::async_runtime::handle().inner().to_owned(),
                            )
                            .await
                            .map_err(|e| (e.0, e.1.change_context(CreateDiaryError)))?;

                        ret = Some(try_with_original!(
                            created_meta_secure(
                                &handle,
                                &decrypted.meta_secure,
                                &decrypted.sections,
                                Arc::clone(&decrypted.storage_access),
                                decrypted.maybe_key.to_owned(),
                            )
                            .await,
                            DiaryState::Decrypted(decrypted),
                            CreateDiaryError
                        ));

                        Ok(DiaryState::Decrypted(decrypted))
                    }
                    _ => Err((
                        state,
                        Report::new(DiaryExists).change_context(CreateDiaryError),
                    )),
                }
            })
            .await?;

        Ok(ret)
    })
    .await
    .flatten()
}

error!(pub DiaryDoesntExist; "Diary doesn't exist");
error!(pub DiaryDecrypted; "Diary already decrypted");
error!(pub DiaryEncrypted; "Diary is encrypted");
error!(pub FailedDecryptingDiary; "Failed to decrypt the diary");

#[derive(Serialize)]
pub enum DecryptDiaryResponse {
    IncorrectKey,
    Decrypted(serde_json::Value),
}

fn test<F: Send>(f: F) -> F {
    f
}

#[tauri::command]
pub async fn decrypt_diary(
    handle: AppHandle,
    password: Secret<String>,
) -> Option<DecryptDiaryResponse> {
    info!("Frontend wants to decrypt the diary");

    send_error_to_frontend(&handle, async {
        let training_interval = config(&handle).read().await.training_interval();

        let mut state = diary_state(&handle).write().await;

        let mut result = None;

        match state
            .update(|state| async {
                match state {
                    DiaryState::Encrypted(encrypted) => {
                        async {
                            let key = match encrypted.meta.encryption() {
                                Some(v) => {
                                    try_with_original!(
                                        v.generate_key(
                                            &encrypted.handle,
                                            Secret::new(
                                                password.expose_secret().as_bytes().to_vec()
                                            ),
                                        )
                                        .await,
                                        DiaryState::Encrypted(encrypted),
                                        FailedDecryptingDiary
                                    )
                                }
                                None => {
                                    return Err((
                                        DiaryState::Encrypted(encrypted),
                                        Report::new(FailedDecryptingDiary).attach_printable(
                                            "Encryption data not present in Meta",
                                        ),
                                    ))
                                }
                            };

                            let handle_for_err = handle.clone();

                            let decrypted =
                                test(encrypted.decrypt(key, training_interval, move |err| {
                                    show_error(&handle_for_err, err)
                                }))
                                .await
                                .map_err(|e| (e.0, e.1.change_context(FailedDecryptingDiary)))?;

                            result = Some(try_with_original!(
                                created_meta_secure(
                                    &handle,
                                    &decrypted.meta_secure,
                                    &decrypted.sections,
                                    Arc::clone(&decrypted.storage_access),
                                    decrypted.maybe_key.to_owned()
                                )
                                .await,
                                DiaryState::Decrypted(decrypted),
                                FailedDecryptingDiary
                            ));

                            Ok(DiaryState::Decrypted(decrypted))
                        }
                        .await
                    }
                    DiaryState::None(v) => Err((
                        DiaryState::None(v),
                        Report::new(DiaryDoesntExist).change_context(FailedDecryptingDiary),
                    )),
                    DiaryState::Decrypted(v) => Err((
                        DiaryState::Decrypted(v),
                        Report::new(DiaryDecrypted).change_context(FailedDecryptingDiary),
                    )),
                }
            })
            .await
        {
            Ok(_) => Ok(match result {
                Some(v) => DecryptDiaryResponse::Decrypted(v),
                None => {
                    return Err(Report::new(FailedDecryptingDiary)
                        .attach_printable("The result should have been set while decrypting"))
                }
            }),
            Err(e) if e.contains::<IncorrectKey>() => Ok(DecryptDiaryResponse::IncorrectKey),
            Err(e) => Err(e.change_context(FailedDecryptingDiary)),
        }
    })
    .await
}

error!(ChangePasswordError; "Failed to change password");

#[tauri::command]
pub async fn change_password(handle: AppHandle, new_password: Option<Secret<String>>) {
    info!(
        "Frontend wants to change the password to {} password",
        match new_password {
            Some(_) => "a new",
            None => "no",
        }
    );

    send_error_to_frontend(&handle, async {
        let mut state = diary_state(&handle).write().await;

        state
            .update(|state| async {
                match state {
                    DiaryState::Decrypted(decrypted) => {
                        let (encryption_state, hashing_data) = match new_password {
                            Some(password) => {
                                let (key, hashing_data) = try_with_original!(
                                    encryption_data_from_password(
                                        &decrypted.handle,
                                        Secret::new(password.expose_secret().as_bytes().to_vec()),
                                        32
                                    )
                                    .await,
                                    DiaryState::Decrypted(decrypted),
                                    ChangePasswordError
                                );

                                (Some(key), Some(hashing_data))
                            }
                            None => (None, None),
                        };

                        match decrypted
                            .change_encryption(encryption_state, hashing_data)
                            .await
                        {
                            Ok(v) => Ok(DiaryState::Decrypted(v)),
                            Err(e) => Err((e.0, e.1.change_context(ChangePasswordError))),
                        }
                    }
                    DiaryState::Encrypted(v) => Err((
                        DiaryState::Encrypted(v),
                        Report::new(DiaryEncrypted).change_context(ChangePasswordError),
                    )),
                    DiaryState::None(v) => Err((
                        DiaryState::None(v),
                        Report::new(DiaryDoesntExist).change_context(ChangePasswordError),
                    )),
                }
            })
            .await
            .change_context(ChangePasswordError)?;

        updated_encryption(
            &handle,
            match &mut *state {
                DiaryState::None(_) => {
                    return Err(Report::new(DiaryDoesntExist).change_context(ChangePasswordError))
                }
                DiaryState::Encrypted(_) => {
                    return Err(Report::new(DiaryEncrypted).change_context(ChangePasswordError))
                }
                DiaryState::Decrypted(decrypted) => decrypted.meta.encryption().is_some(),
            },
        )
        .change_context(ChangePasswordError)
    })
    .await;
}
