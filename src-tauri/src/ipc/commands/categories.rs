use error_stack::{IntoReport, Report, ResultExt};
use error_stack_error_macro::error;
use log::trace;
use tauri::AppHandle;

use crate::{ipc::updated_filters_convenient as updated_filters, update_meta_secure};

error!(pub CategoryDoesntExist { pub index: u32 } "Category number {index} doesn't exist");

#[macro_export]
macro_rules! update_category {
    ($handle: expr, $context: expr, $id: expr, |$decrypted: ident, $meta_secure: ident, $category: ident| $stuff: expr) => {
        update_meta_secure!($handle, $context, |$decrypted, $meta_secure| {
            let $category = match $meta_secure
                .categories
                .get_category_mut($id)
                .ok_or_else(|| Report::new($crate::ipc::CategoryDoesntExist { index: $id }))
                .change_context($context)
            {
                Ok(c) => c,
                Err(e) => return ($meta_secure, Err(e)),
            };

            $stuff
        })
    };
}

error!(pub CreateCategoryError; "Creating category failed");

#[tauri::command]
pub async fn create_category(handle: AppHandle, name: String) -> Option<serde_json::Value> {
    trace!("Frontend wants to create a category");

    update_meta_secure!(&handle, CreateCategoryError, |decrypted, meta_secure| {
        meta_secure.categories.create_category(name);

        serde_json::to_value(&meta_secure.categories)
            .into_report()
            .change_context(CreateCategoryError)
    })
}

error!(pub ChangeCategoryParentError; "Changing category parent failed");

#[tauri::command]
pub async fn change_category_parent(
    handle: AppHandle,
    new_child_id: u32,
    new_parent_id: Option<u32>,
) -> Option<serde_json::Value> {
    trace!("Frontend wants to move a category");

    let value = update_category!(
        &handle,
        ChangeCategoryParentError,
        new_child_id,
        |decrypted, meta_secure, category| {
            category.set_parent(new_parent_id);

            serde_json::to_value(&meta_secure.categories)
                .into_report()
                .change_context(ChangeCategoryParentError)
        }
    );

    updated_filters(&handle).await;

    value
}

error!(pub ChangeCategoryColorError; "Changing category color failed");

#[tauri::command]
pub async fn change_category_color(handle: AppHandle, category_id: u32, color: String) {
    trace!("Frontend wants to change a category color");

    update_category!(
        &handle,
        ChangeCategoryColorError,
        category_id,
        |decrypted, meta_secure, category| {
            category.set_color(color);
            Ok(())
        }
    );
}

error!(pub RenameCategoryError; "Renaming category failed");

#[tauri::command]
pub async fn rename_category(handle: AppHandle, category_id: u32, name: String) {
    trace!("Frontend wants to rename a category");

    update_category!(
        &handle,
        RenameCategoryError,
        category_id,
        |decrypted, meta_secure, category| {
            category.set_name(name);
            Ok(())
        }
    );
}

error!(pub DeleteCategoryError; "Deleting category failed");

#[tauri::command]
pub async fn delete_category(handle: AppHandle, category_id: u32) -> Option<serde_json::Value> {
    trace!("Frontend wants to delete a category");

    let value = update_meta_secure!(&handle, DeleteCategoryError, |decrypted, meta_secure| {
        meta_secure.delete_category(category_id);

        serde_json::to_value(&meta_secure.categories)
            .into_report()
            .change_context(DeleteCategoryError)
    });

    updated_filters(&handle).await;

    value
}

error!(pub SetCategoryClosedError; "Deleting category failed");

#[tauri::command]
pub async fn set_category_closed(handle: AppHandle, category_id: u32, closed: bool) {
    trace!(
        "Frontend wants to {} a category",
        match closed {
            true => "close",
            false => "open",
        }
    );

    update_category!(
        &handle,
        SetCategoryClosedError,
        category_id,
        |decrypted, meta_secure, category| {
            category.set_closed(closed);
            Ok(())
        }
    );
}
