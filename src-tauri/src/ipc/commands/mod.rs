mod categories;
mod filters;
mod sections;
mod settings;
mod state;

pub use categories::*;
pub use filters::*;
pub use sections::*;
pub use settings::*;
pub use state::*;
use tauri::{updater::UpdateResponse, AppHandle, Manager, Wry};

use error_stack::IntoReport;

use super::send_error_to_frontend;

#[tauri::command]
pub async fn install_update(handle: AppHandle) {
    #[cfg(feature = "auto-updates")]
    {
        send_error_to_frontend(&handle, async {
            handle
                .state::<UpdateResponse<Wry>>()
                .inner()
                .to_owned()
                .download_and_install()
                .await
                .into_report()
        })
        .await;
    }
}
