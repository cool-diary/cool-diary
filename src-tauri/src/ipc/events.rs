use std::{path::Path, sync::Arc};

use electra::{NiceErrorMessage, Reference, StorageAccess};
use error_stack::{Context, IntoReport, Report, Result, ResultExt};
use error_stack_error_macro::error;
use futures::{stream, StreamExt, TryStreamExt};
use lib_nebulous::{Key, MetaSecure, Sections};
use log::{info, trace};
use serde::Serialize;
use serde_json::json;
use tauri::{AppHandle, Manager, Runtime};

use crate::try_get_decrypted;

use super::send_error_to_frontend;

pub fn show_error<R: Runtime, E: Context>(handle: &AppHandle<R>, err: Report<E>) {
    log::error!("{err}");

    if let Err(e) = handle.emit_all(
        "nebulous://error",
        json!({
            "message": err.downcast_ref::<NiceErrorMessage>().cloned(),
            "details": format!("{err:#}"),
        }),
    ) {
        log::error!("Sending an error to the GUI failed");

        tauri::api::dialog::message(
            handle.get_window("main").as_ref(),
            "Sending an error to the GUI failed",
            format!("Sending an error to the GUI failed: {e}\nOriginal error: {err:#}"),
        );
    }
}

error!(pub AnnounceMetaSecureFailed; "Failed to announce that MetaSecure is available");

pub async fn created_meta_secure<
    'a,
    SA: StorageAccess<FileKey = Reference<Path>, DataType = Vec<u8>> + ?Sized,
>(
    handle: &AppHandle,
    meta_secure: &'a MetaSecure,
    sections: &Sections<SA>,
    sa: Arc<SA>,
    key: Option<Key>,
) -> Result<serde_json::Value, AnnounceMetaSecureFailed> {
    info!("Generating initial decrypted data for the frontend");

    updated_encryption(handle, key.is_some()).change_context(AnnounceMetaSecureFailed)?;

    let mut sections_read = stream::iter(sections.iter())
        .then(|(file_key, v)| {
            let sa = Arc::clone(&sa);
            let key = key.to_owned();

            async move {
                Ok::<_, Report<AnnounceMetaSecureFailed>>((
                    serde_json::to_string(&file_key)
                        .into_report()
                        .change_context(AnnounceMetaSecureFailed)?,
                    &**v.get(sa, key.to_owned(), key)
                        .await
                        .change_context(AnnounceMetaSecureFailed)?,
                ))
            }
        })
        .try_collect::<Vec<_>>()
        .await?;

    sections_read.sort_unstable_by(|a, b| b.1.ordinal().cmp(&a.1.ordinal()));

    Ok(json!({
        "filters": {
            "selectedCategories": meta_secure.filters.category_selector(),
            "timeOption": meta_secure.filters.time(),
            "textSearch": meta_secure.filters.text_search(),
            "categoryFilterType": meta_secure.filters.category_filter_type(),
        },
        "sections": sections_read,
        "categories": meta_secure.categories,
        "sectionsPassingFilters": sections_passing_filters(meta_secure, sections, sa, key).await.change_context(AnnounceMetaSecureFailed)?
    }))
}

error!(pub SectionsPassingFiltersError; "Failed to get which sections pass the filters");

async fn sections_passing_filters<
    SA: StorageAccess<FileKey = Reference<Path>, DataType = Vec<u8>> + ?Sized,
>(
    meta_secure: &MetaSecure,
    sections: &Sections<SA>,
    sa: Arc<SA>,
    key: Option<Key>,
) -> Result<impl Serialize + Clone, SectionsPassingFiltersError> {
    let sections_passing = meta_secure
        .sections_passing_filter(sa, sections, key)
        .await
        .change_context(SectionsPassingFiltersError)?;

    sections_passing
        .iter()
        .map(serde_json::to_string)
        .collect::<core::result::Result<Vec<String>, _>>()
        .into_report()
        .change_context(SectionsPassingFiltersError)
}

error!(pub AnnounceUpdatedFiltersFailed; "Failed to announce that filters were updated");

pub async fn updated_filters<
    SA: StorageAccess<FileKey = Reference<Path>, DataType = Vec<u8>> + ?Sized,
>(
    handle: &AppHandle,
    meta_secure: &MetaSecure,
    sections: &Sections<SA>,
    sa: Arc<SA>,
    key: Option<Key>,
) -> Result<(), AnnounceUpdatedFiltersFailed> {
    trace!("Sending the sections passing the filters to the frontend");

    handle
        .emit_all(
            "nebulous://updated-sections-passing-filter",
            sections_passing_filters(meta_secure, sections, sa, key)
                .await
                .change_context(AnnounceUpdatedFiltersFailed)?,
        )
        .into_report()
        .change_context(AnnounceUpdatedFiltersFailed)?;

    Ok(())
}

pub async fn updated_filters_convenient(handle: &AppHandle) {
    send_error_to_frontend(handle, async {
        try_get_decrypted!(handle, assign to decrypted, AnnounceUpdatedFiltersFailed);

        updated_filters(
            handle,
            &decrypted.meta_secure,
            &decrypted.sections,
            Arc::clone(&decrypted.storage_access),
            decrypted.maybe_key.to_owned(),
        )
        .await
        .change_context(AnnounceUpdatedFiltersFailed)
    })
    .await;
}

error!(pub AnnounceEncryptionFailed; "Failed to announce that encryption was updated");

pub fn updated_encryption(
    handle: &AppHandle,
    encryption: bool,
) -> Result<(), AnnounceEncryptionFailed> {
    trace!("Sending the encryption state to the frontend");

    handle
        .emit_all("nebulous://update-encryption", encryption)
        .into_report()
        .change_context(AnnounceEncryptionFailed)?;

    Ok(())
}
