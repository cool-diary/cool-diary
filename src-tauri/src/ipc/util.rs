use std::future::Future;

use electra::{BufferingStorageAccess, DiskStorageAccess, Manager};
use error_stack::Report;
use error_stack_error_macro::error;
use lib_nebulous::{
    Categories, CategoryFilterType, CategorySelector, Config, DiaryState, Filters, MetaSecure,
};
use log::Level;
use serde::Deserialize;
use tauri::AppHandle;
use tokio::sync::RwLock;

use crate::settings::Settings;

use super::events;

#[derive(Deserialize)]
#[serde(rename_all = "UPPERCASE")]
pub enum LogLevel {
    Error,
    Warn,
    Info,
    Debug,
    Trace,
}

#[tauri::command]
pub fn log(level: LogLevel, target: &str, msg: String) {
    log::log!(
        target: target,
        match level {
            LogLevel::Error => Level::Error,
            LogLevel::Warn => Level::Warn,
            LogLevel::Info => Level::Info,
            LogLevel::Debug => Level::Debug,
            LogLevel::Trace => Level::Trace,
        },
        "{msg}"
    );
}

pub fn default_meta_secure() -> MetaSecure {
    let mut categories = Categories::default();
    categories.create_category("Journal".to_owned());

    MetaSecure::new(
        categories,
        Filters::new(CategorySelector::new(), CategoryFilterType::All, None),
    )
}

error!(pub DiaryNotDecrypted; "Diary isn't decrypted");

pub async fn send_error_to_frontend<
    T,
    E: error_stack::Context,
    F: Future<Output = Result<T, Report<E>>>,
>(
    handle: &AppHandle,
    stuff: F,
) -> Option<T> {
    match stuff.await {
        Ok(v) => Some(v),
        Err(err) => {
            events::show_error(handle, err);

            None
        }
    }
}

#[macro_export]
macro_rules! try_get_decrypted {
    ($handle: expr, assign to $name: ident, $context: expr) => {
        let state = $crate::ipc::util::diary_state($handle).read().await;

        let $name = match &*state {
            lib_nebulous::DiaryState::Decrypted(decrypted) => decrypted,
            _ => {
                return Err(error_stack::Report::new($crate::ipc::DiaryNotDecrypted)
                    .change_context($context));
            }
        };
    };

    ($handle: expr, assign to mut $name: ident, $context: expr) => {
        let mut state = $crate::ipc::util::diary_state($handle).write().await;

        let $name = match &mut *state {
            lib_nebulous::DiaryState::Decrypted(decrypted) => decrypted,
            _ => {
                return Err(error_stack::Report::new($crate::ipc::DiaryNotDecrypted)
                    .change_context($context));
            }
        };
    };
}

#[macro_export]
macro_rules! update_decrypted {
    ($handle: expr, $context: expr, |$decrypted: ident| $stuff: expr) => {
        $crate::ipc::util::send_error_to_frontend($handle, async {
            $crate::try_get_decrypted!($handle, assign to mut $decrypted, $context);

            $stuff
        }).await
    }
}

#[macro_export]
macro_rules! update_meta_secure {
    ($handle: expr, $context: expr, |$decrypted: ident, $meta_secure: ident| $stuff: expr) => {
        $crate::update_decrypted!($handle, $context, |$decrypted| {
            $decrypted
                .meta_secure
                .write($decrypted.maybe_key.to_owned(), |mut $meta_secure| async {
                    let v = $stuff;

                    ($meta_secure, v)
                })
                .await
                .change_context($context)?
        })
    };
}

pub type SAUsing = BufferingStorageAccess<DiskStorageAccess>;
pub type ConfigSAUsing = DiskStorageAccess;

pub fn diary_state(handle: &AppHandle) -> &'_ RwLock<DiaryState<SAUsing>> {
    <AppHandle as tauri::Manager<_>>::state::<RwLock<DiaryState<SAUsing>>>(handle).inner()
}

pub fn config(handle: &AppHandle) -> &'_ RwLock<Manager<Config<Settings>, ConfigSAUsing>> {
    <AppHandle as tauri::Manager<_>>::state::<RwLock<Manager<Config<Settings>, ConfigSAUsing>>>(
        handle,
    )
    .inner()
}
