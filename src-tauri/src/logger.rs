use crossbeam_queue::SegQueue;
use serde_json::json;
use std::{
    borrow::Cow,
    sync::atomic::{AtomicUsize, Ordering},
};

use chrono::Local;
use log::{info, Level, LevelFilter};
use owo_colors::OwoColorize;

use tauri::{AppHandle, Manager};
use tokio::sync::OnceCell;

pub struct Logger<R: tauri::Runtime> {
    max_target_len: AtomicUsize,
    min_level: Level,
    frontend_queue: SegQueue<(usize, serde_json::Value)>,
    app_handle: OnceCell<AppHandle<R>>,
}

impl<R: tauri::Runtime> Logger<R> {
    pub const fn new(min_level: Level) -> Self {
        Self {
            max_target_len: AtomicUsize::new(0),
            min_level,
            frontend_queue: SegQueue::new(),
            app_handle: OnceCell::const_new(),
        }
    }

    pub fn install(&'static self) {
        info!("Installing the logger");
        log::set_logger(self).expect("no other logger was loaded first");
        log::set_max_level(match self.min_level {
            Level::Error => LevelFilter::Error,
            Level::Warn => LevelFilter::Warn,
            Level::Info => LevelFilter::Info,
            Level::Debug => LevelFilter::Debug,
            Level::Trace => LevelFilter::Trace,
        });
    }

    pub fn set_app_handle(&self, app_handle: tauri::AppHandle<R>) {
        info!("Adding app handle to logger");
        while let Some((max, log_json)) = self.frontend_queue.pop() {
            Self::send_log_json_to_frontend(log_json, &app_handle, max);
        }

        drop(self.app_handle.set(app_handle));
    }

    fn send_log_json_to_frontend(
        log_json: serde_json::Value,
        app_handle: &tauri::AppHandle<R>,
        max: usize,
    ) {
        if let Err(e) = app_handle.emit_all("nebulous://log", log_json) {
            #[cfg(debug_assertions)]
            print!(
                "{} {} {:<max$}  {} Failed to send log message to the frontend: {}",
                Self::now().bright_black(),
                "ERROR".red(),
                "logger".bold(),
                ">".bright_black(),
                e,
            );
        }
    }

    fn now() -> String {
        Local::now().format("%H:%M:%S%.3f").to_string()
    }
}

impl<R: tauri::Runtime> log::Log for Logger<R> {
    fn enabled(&self, meta: &log::Metadata) -> bool {
        meta.level() <= self.min_level
    }

    // Replicate pretty_env_logger, bc i like it
    fn log(&self, record: &log::Record) {
        if !self.enabled(record.metadata()) {
            return;
        }

        let max = self
            .max_target_len
            .fetch_max(record.target().len(), Ordering::Relaxed);

        let time = Self::now();

        let args = record.args();
        let message = match args.as_str() {
            Some(v) => Cow::Borrowed(v),
            None => Cow::Owned(format!("{args}")),
        };

        #[cfg(debug_assertions)]
        {
            print!("{} ", time.bright_black());

            match record.level() {
                Level::Error => print!("{} ", "ERROR".red()),
                Level::Warn => print!("{} ", "WARN ".yellow()),
                Level::Info => print!("{} ", "INFO ".green()),
                Level::Debug => print!("{} ", "DEBUG".blue()),
                Level::Trace => print!("{} ", "TRACE".magenta()),
            }

            println!(
                "{:<max$}  {} {}",
                record.target().bold(),
                ">".bright_black(),
                message
            )
        }

        let log_json = json!({
            "level": match record.level() {
                Level::Error => 5,
                Level::Warn => 4,
                Level::Info => 3,
                Level::Debug => 2,
                Level::Trace => 1,
            },
            "time": time,
            "module": record.target(),
            "message": message,
        });

        if let Some(app_handle) = self.app_handle.get() {
            Self::send_log_json_to_frontend(log_json, app_handle, max);
        } else {
            self.frontend_queue.push((max, log_json));
        }
    }

    fn flush(&self) {}
}
