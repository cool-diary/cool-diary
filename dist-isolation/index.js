const allowedCommands = [
    "__initialized",
    "ready",
    "open_docs",
    "data_state",
    "create_diary",
    "decrypt_diary",
    "change_password",
    "update_closed_categories",
    "add_category_to_filters",
    "remove_category_from_filters",
    "update_time_option",
    "update_text_search",
    "set_category_filter_option",
    "create_category",
    "change_category_parent",
    "change_category_color",
    "rename_category",
    "delete_category",
    "set_category_closed",
    "add_category_to_section",
    "remove_category_from_section",
    "create_section",
    "delete_section",
    "update_section_text",
    "update_section_title",
    "update_section_time",
    "update_section_categories",
    "update_language",
    "update_category_prediction",
    "update_check_for_updates",
    "install_update",
    "initialized",
    "get_text",
]

const allowedModules = ["Event", "Clipboard"]

window.__TAURI_ISOLATION_HOOK__ = payload => {
    if (
        allowedCommands.includes(payload.cmd) ||
        (payload.cmd === "tauri" &&
            allowedModules.includes(payload.__tauriModule))
    ) {
        console.log("ALLOWED: ", payload)
        return payload
    }

    let payloadStr = JSON.stringify(payload)

    return {
        cmd: "log",
        target: "Isolation",
        level: "WARN",
        msg: `REJECTED: ${payloadStr}`,
        callback: payload.callback,
        error: payload.error,
    }
}
