# How does it work?

Nebulous converts you password into an encryption key, and uses it to encrypt/decrypt your files.

This means that anyone who only has access to the files can't know what you're writing about.

### Technical details

Requirement | Algorithm
-|-
Key derivation | [bcrypt-pbkdf](https://crates.io/crates/bcrypt-pbkdf)
Encryption | [aes-gcm-siv](https://crates.io/crates/aes-gcm-siv)
