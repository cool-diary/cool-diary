# What doesn't it protect?

- The last time you opened the app
- The number of sections you've written
- A rough estimate of how much data is contained in each section
- If the attacker has continuous access to the diary (cloud syncing), they can find out when sections are created/modified (by observing as they change in real time)