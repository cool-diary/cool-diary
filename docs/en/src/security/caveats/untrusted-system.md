# Untrusted system

If you don't trust your operating system, then Nebulous can't protect your data

### System admins

An administrator is capable of directly reading the memory of any programs running on your computer, exposing all the data stored in your diary

As a high schooler, I can tell you that our school uses [Securly](https://securly.com) to spy on students. Its capabilities are not clear to me, but judging from their website, it logs all of the student's keystrokes, livestreams the computer's screen to administrators, and uses machine learning to flag you for problems.

### Viruses

If your computer has viruses, your data is insecure.

Logging all of your keystrokes (and broadcasting them to whomever) generally doesn't require admin permissions, and is a very typical thing for malware to do.

Make sure you keep your computer clean, and don't download weird crap

