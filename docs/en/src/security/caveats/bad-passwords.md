# Bad passwords

### Weak passwords

Of course, if you have a short password that's easy to guess, there isn't a whole lot Nebulous can do.

My suggestion would be to use a password manager like [Bitwarden](https://bitwarden.com) to secure your passwords, and use it's password generator to generate a 4 word passphrase. [xkcd comic](https://xkcd.com/936/)

### Compromised passwords

Reusing passwords is equivalent to telling other people your password, and is equally bad an idea. Again, please use a password manager.

If you reuse your google password for your microsoft account, microsoft now (in theory) can sign into your google account. That doesn't normally happen, but the risk is attackers hacking into microsoft, grabbing your password hash, and cracking it.

### No password

Obviously, if you don't use a password, Cool Diary can't encrypt your data, leaving it unprotected.
