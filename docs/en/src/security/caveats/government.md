# The government

### Legal caveats

In the United States, the government can compel you to give up your password if they have a warrant.

This doesn't mean encryption is useless, because the government would have to establish probable cause & be specific with what they ask to find (unless you give them permission).

I'm not a lawyer and this could be totally wrong. I also can't speak for other countries.

[https://www.kennethballard.com/?p=4152](https://www.kennethballard.com/?p=4152)

### Technical caveats

Another factor is that governments have far more resources to try to hack your diary, or to hack you.

The best example of this that I know of is [Pegasus](https://en.wikipedia.org/wiki/Pegasus_(spyware)). 

