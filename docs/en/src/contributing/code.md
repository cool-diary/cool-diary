# Code

Nebulous is split into two different crates:

## [nebulous](https://gitlab.com/nebulous-app/nebulous)

This crate uses [Tauri](https://tauri.app) to glue all the various components together into a unified desktop app.

## [lib-nebulous](https://gitlab.com/nebulous/lib-nebulous)

This crate implements the business logic of Nebulous.

This is seperated from the GUI to allow for separation of concerns.

If you wanted to, you could use it as a dependency and create your own version of Nebulous (there are no stability guarantees).

## [nebulous-svelte-ui](https://gitlab.com/nebulous-app/nebulous-svelte-ui)

This crate implements the Nebulous GUI. It uses [Svelte](https://svelte.dev) as the GUI library. Communicates with the backend via a well defined interface that dependents can implement.

## [electra](https://gitlab.com/nebulous-app/electra)

A generic file synchronization library used by lib-nebulous to automatically synchronize data with the user's storage.
