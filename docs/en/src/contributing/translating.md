# Translations

## How to translate the app

### Download the translation you want to start from

You can download any existing translation at [https://gitlab.com/nebulous-app/nebulous/-/tree/main/translations](https://gitlab.com/nebulous-app/nebulous/-/tree/main/translations)

### Edit the translations in a text editor

The syntax should be self explanatory. Using a code editor is recommended because you get syntax highlighting.

You might need to quote text if it uses special characters. Typically, this isn't neccessary.

### Send me the translation

If you know how to make a merge request in gitlab, please do so.

If you don't, that's alright too. You can send me the file via email: [h.rovnyak@proton.me](mailto:h.rovnyak@proton.me)

## How to translate the documentation

### Download the translation you want to start from

You can download any existing translation at [https://gitlab.com/nebulous-app/nebulous/-/tree/main/docs](https://gitlab.com/nebulous-app/nebulous/-/tree/main/docs)

You'll need to download the whole directory, you can do that with the download icon to the left of the blue "Clone" icon

### Edit the documentation in a text editor

To start off, you'll want to edit `book.toml`
- Add your name to `authors` (`authors = ["Henry Rovnkak", "your name"]`)
- Replace the language with the language you're translating to (`language = "whatever"`)
- Translate the title (`title = "Whatever"`)

Then, go into the source directory, and translate all the `.md` files there.

The files are written using [Markdown](https://commonmark.org/help/) with some [extensions](https://rust-lang.github.io/mdBook/format/markdown.html#extensions)

### Send me the translation

If you know how to make a merge request in gitlab, please do so.

If you don't, that's alright too. You can zip the folder & send it to me via email: [h.rovnyak@proton.me](mailto:h.rovnyak@proton.me)
