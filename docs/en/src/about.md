# Introduction

This book is meant to answer questions about the app that aren't really obvious.

This won't cover how to use the app. If you don't know, file a bug report because my intention is to make all the features easily discoverable

## Source code

[https://gitlab.com/nebulous-app](https://gitlab.com/nebulous-app)