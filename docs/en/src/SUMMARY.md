# Summary

- [Introduction](./about.md)
- [License](./gpl-3.0.md)
- [Credits](./credits.md)

- [Security](./security.md)
  - [How does it work?](./security/how-does-it-work.md)
  - [What does it protect?](./security/what-does-it-protect.md)
  - [What doesn't it protect?](./security/what-doesnt-it-protect.md)
  - [Caveats](./security/caveats.md)
    - [The government](./security/caveats/government.md)
    - [Untrusted system](./security/caveats/untrusted-system.md)
    - [Bad passwords](./security/caveats/bad-passwords.md)
  
- [Contributing](./contributing.md)
  - [Translations](./contributing/translating.md)
  - [Code](./contributing/code.md)
  - [Ideas / Bug reports](./contributing/ideas.md)
