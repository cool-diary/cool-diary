# Ideas, bug reports, suggestions, etc.

You can contribute these by opening an issue at [https://gitlab.com/nebulous-app/nebulous/-/issues](https://gitlab.com/nebulous-app/nebulous/-/issues)