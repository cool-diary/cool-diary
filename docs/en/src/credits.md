## Contributors

Name|What they did
-|-
Henry Rovnyak | Main contributor
Nora Searles | Suggesting the name "Nebulous"

## Attributions

Name|What I use it for|License
-|-|-
[Icofont](https://www.icofont.com/) | Icons | [https://www.icofont.com/license](https://www.icofont.com/license)
