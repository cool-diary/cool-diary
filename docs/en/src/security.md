# Security

One of Nebulous's main goals is protecting your data from attackers, however there are some caveats & it doesn't protect everything.

Also, *nothing* is protected if you use a weak password or no password.