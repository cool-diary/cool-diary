# Nebulous

The idea is that you can have a diary, except it's encrypted so no-one will ever be able to read it without your password.

It's also designed to be better organized than other solutions. Entries are organized into sections, to each of which you can assign a title, categories, and a date, allowing you to break up your entries into multiple topics.
For example, on one day, you could write about what you did in a section under the `Journal` category, and write about a cool new idea you had in an `Ideas` section, while assigning them the same date.
If you organize like that, you could search your diary just for your ideas, without having to skim through unrelated details.

You can also have subcategories under broader categories. For example, you could have a `Programming ideas` subcategory under the `Ideas` category. That gives you better organization by allowing you to search over all your ideas or just your programming ideas.

## Download:

[https://gitlab.com/nebulous-app/nebulous/-/releases](https://gitlab.com/nebulous-app/nebulous/-/releases)

For linux, I'm planning on releasing a flatpak whenever Nebulous becomes reasonably stable, and I figure out how to do that.

On MacOS, Google Chrome screws up the download for some reason, so download it with safari or something. If that happens on other OSes try a different browser. Also, it'll give an error saying it won't open because it's from an "unidentified developer". Go to settings, privacy and security, then click open. You shouldn't get the error again.
